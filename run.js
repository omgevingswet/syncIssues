'use strict'
const rp = require('request-promise')
const fs = require('fs')
const path = require('path')
const gherkin = require('gherkin')

const baseUrl = 'https://gitlab.com/api/v3/'
const projectId = 3262759
const token = 'NTz6Ep4LqDt2m96TEQqx'
const branch = 'master'
const jobName = "test:node:6"

const testFile = "test_result.json"
const featureDir = "../../requirements/bestaande-documenten"

const getLatestTestResults = () => {
    if (fs.existsSync(testFile)) {
        return new Promise((fulfill, reject) => {
            fs.readFile(testFile, (err, data) => {
                if (err) {
                    return reject(err)
                }
                fulfill(JSON.parse(data))
            })
        })
    }
    return rp({
        url: `${baseUrl}projects/${projectId}/pipelines`,
        json: true,
        headers: {'PRIVATE-TOKEN': token }
    }).then((pipelines) => {
        for (const p in pipelines) {
            const pipeline = pipelines[p]
            if (pipeline.ref === branch) {
                return pipeline.id
            }
        }
    }).then((pipelineId) => {
        return rp({
            url: `${baseUrl}projects/${projectId}/pipelines/${pipelineId}/jobs`,
            json: true,
            headers: {'PRIVATE-TOKEN': token }
        })
    }).then((jobs) => {
        for (const j in jobs) {
            const job = jobs[j]
            if (job.name === jobName) {
                return job.id
            }
        }
    }).then((jobId) => {
        return rp({
            url: `${baseUrl}projects/${projectId}/jobs/${jobId}/artifacts`,
            encoding: null,
            headers: {'PRIVATE-TOKEN': token }
        })
    }).then((artifact) => {
        const JSZip = new require('node-zip')
        const zip = new JSZip(artifact, {base64: false, checkCRC32: true})
        const data = zip.file(testFile)._data
        fs.writeFileSync(testFile, data)
        return JSON.parse(data)
    })
}

const getIssues = (previous) => {
    previous = previous || []
    if (previous.length % 100 !== 0) {
        // last page
        return new Promise((fulfill, _) => {
            fulfill(previous)
        })
    }
    return rp({
        url: `${baseUrl}projects/${projectId}/issues?per_page=100`,
        json: true,
        headers: {'PRIVATE-TOKEN': token }
    }).then((json) => {
        return getIssues(previous.concat(json))
    })
}

const readFeatures = () => {
    return new Promise((fulfill, reject) => {
        fs.readdir(featureDir, (err, files) => {
            if (err) {
                return reject(err)
            }
            files = files.map((f) => path.join(featureDir, f))
            fulfill(files.filter((f) => f.endsWith(".feature")))
        })
    }).then((files) => {
        const readPromises = files.map((file) => {
            return new Promise((fulfill, reject) => {
                fs.readFile(file, "utf8", (err, data) => {
                    if (err) {
                        return reject(err)
                    }
                    const parser = new gherkin.Parser()
                    const feature = parser.parse(data)
                    fulfill({file, feature})
                })
            })
        })
        return Promise.all(readPromises)
    })
}

const getScenarios = (featureFiles) => {
    // TODO
    return []
}

const findTests = (scenario, testResults) => {
    // TODO
}

const findMissingTestIssue = (scenario, issues) => {
    // TODO
}

const createMissingTestIssue = (scenario) => {
    // TODO
}

const reopenIssue = (issue) => {
    // TODO
}

const findFailingTestIssue = (failure) => {
    // TODO
}

const createFailingTestIssue = (failure) => {
    // TODO
}

Promise.all([getLatestTestResults(), getIssues(), readFeatures()])
        .then(([testResults, issues, featureFiles]) => {

    // (re)open issues for scenarios that have not tests
    const scenarios = getScenarios(featureFiles)
    for (const scenario in scenarios) {
        const tests = findTests(scenario, testResults)
        if (!tests) {
            const issue = findMissingTestIssue(scenario, issues)
            if (!issue) {
                createMissingTestIssue(scenario)
            } else if (!issue.open) {
                reopenIssue(issue)
            }
        }
    }

    // (re)open issues for failing tests
    for (const failure in testResults.failures) {
        const issue = findFailingTestIssue(failure, issues)
        if (!issue) {
            createFailingTestIssue(failure)
        } else if (!issue.open) {
            reopenIssue(issue)
        }
    }

}).catch((error) => {
    console.log(error)
})
